﻿using System.Collections.Generic;

namespace TinhDiemTrungBinh
{
    class StudentInfo
    {
        public long MaSV { get; set; }
        public string MaLop { get; set; }
        public string TenSV { get; set; }
        public string NgaySinh { get; set; }
        public float DiemTB { get; set; }
        public Dictionary<string, MarkStructure> DS_MonHoc = new Dictionary<string, MarkStructure>();

        public StudentInfo()
        {
        }
    }
}
