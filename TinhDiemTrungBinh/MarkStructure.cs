﻿namespace TinhDiemTrungBinh
{
    class MarkStructure
    {
        public SubjectInfo Sbj_Info { get; set; }
        public float DiemCC { get; set; }//Điểm chuyên cần
        public float DiemGK { get; set; }//Điểm giữa kì
        public float DiemHK { get; set; }//Điểm học kì
        public float DiemTK { get; set; }//Điểm tổng kết

        public MarkStructure()
        {
        }
    }
}
