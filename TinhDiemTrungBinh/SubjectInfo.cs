﻿namespace TinhDiemTrungBinh
{
    class SubjectInfo
    {
        public string MaMon { get; set; }//Mã môn học
        public string TenMon { get; set; }//Tên môn học
        public int SoDVHT { get; set; }//Số đơn vị học trình

        public SubjectInfo(string MaMon, string TenMon, int SoDVHT)
        {
            this.MaMon = MaMon;
            this.TenMon = TenMon;
            this.SoDVHT = SoDVHT;
        }
    }
}
