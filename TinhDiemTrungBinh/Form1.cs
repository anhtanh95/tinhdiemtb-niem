﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace TinhDiemTrungBinh
{
    public partial class Form1 : Form
    {
        string dataPath = Environment.CurrentDirectory + "/Data";
        Dictionary<string, List<string[]>> danhSachMon = new Dictionary<string, List<string[]>>();
        Dictionary<string, SubjectInfo> thongTinMon = new Dictionary<string, SubjectInfo>();
        Dictionary<long, StudentInfo> DsSV = new Dictionary<long, StudentInfo>();
        BindingSource BindingSrc = new BindingSource();
        BindingSource BindingSrc_HocBong = new BindingSource();
        int tongSoDVHT = 0;

        public Form1()
        {
            InitializeComponent();
            comboBox1.SelectedItem = "Tất Cả";
            comboBox2.SelectedItem = "Tất Cả";
            textBox1.Text = dataPath;
            folderBrowserDialog1.SelectedPath = dataPath;
            dataGridView1.Columns["DiemTK"].ValueType = typeof(float);
        }

        public bool loadingDataFromFile()
        {
            if (!File.Exists(dataPath + "/SUBJECTINFO.txt"))
            {
                MessageBox.Show("Could not found file SUBJECTINFO.TXT", "Loading File Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            string[] filePaths = Directory.GetFiles(@"" + dataPath, "*.txt", SearchOption.AllDirectories);
            foreach (string filePath in filePaths)
            {
                string FileName = Path.GetFileNameWithoutExtension(filePath).ToUpper();
                string[] lines = File.ReadAllLines(@"" + filePath);
                if (lines.Length <= 0)
                {
                    MessageBox.Show("File " + FileName + " is empty!", "Loading File Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                if (FileName.Equals("SUBJECTINFO"))
                {
                    //Load dữ liệu trong file: Mã Môn Học|Tên Môn Học|Số Đơn Vị Học Trình
                    foreach (string cellValues in lines)
                    {
                        string[] SubjectInfo = cellValues.Split(new[] { '|' });
                        thongTinMon[SubjectInfo[0]] = new SubjectInfo(SubjectInfo[0], SubjectInfo[1], Int32.Parse(SubjectInfo[2]));
                    }
                }
                else
                {
                    string[] fileNameInfo = FileName.Split(new[] { '_' });
                    string className = fileNameInfo[0];
                    string maMonHoc = fileNameInfo[1];

                    //Load tiếp danh sách theo mã môn nếu đã tồn tại, còn không tạo mới
                    List<string[]> danhSachSV = new List<string[]>();
                    if (danhSachMon.ContainsKey(maMonHoc))
                    {
                        danhSachMon.TryGetValue(maMonHoc, out danhSachSV);//Load if exists
                    }

                    //Load dữ liệu trong file
                    foreach (string cellValues in lines)
                    {
                        string[] cellArray = cellValues
                            .Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        StudentInfo stdInfo;

                        long MaSV = long.Parse(cellArray[1]);
                        if (!DsSV.ContainsKey(MaSV))
                        {
                            stdInfo = new StudentInfo();
                            stdInfo.MaLop = className;//MaLop
                            stdInfo.MaSV = MaSV;//MaSV

                            string name = "";
                            int i;
                            for (i = 2; i < cellArray.Length - 5; i++)
                            {
                                name = name + " " + cellArray[i];
                            }
                            stdInfo.TenSV = name.Substring(1, name.Length - 1);//HoTen

                            stdInfo.NgaySinh = cellArray[i++];//NgaySinh
                        }
                        else
                        {
                            stdInfo = DsSV[MaSV];
                        }

                        MarkStructure subjectMark = new MarkStructure();
                        int length = cellArray.Length;
                        if (maMonHoc.Equals("TTCS") || maMonHoc.Equals("TTTN"))
                        {
                            subjectMark.DiemCC = 0;//DiemCC
                            subjectMark.DiemGK = 0;//DiemGK
                            subjectMark.DiemHK = float.Parse(cellArray[length - 2]);//DiemHK
                            subjectMark.DiemTK = float.Parse(cellArray[length - 1]);//DiemTK
                        }
                        else
                        {
                            subjectMark.DiemCC = float.Parse(cellArray[length - 4]);//DiemCC
                            subjectMark.DiemGK = float.Parse(cellArray[length - 3]);//DiemGK
                            subjectMark.DiemHK = float.Parse(cellArray[length - 2]);//DiemHK
                            subjectMark.DiemTK = (int)(((subjectMark.DiemCC * 10f) / 100f
                                + (subjectMark.DiemGK * 20f) / 100f
                                + (subjectMark.DiemHK * 70f) / 100f) + 0.5);//DiemTK
                        }
                        subjectMark.Sbj_Info = thongTinMon[maMonHoc];

                        stdInfo.DS_MonHoc[maMonHoc] = subjectMark;

                        DsSV[MaSV] = stdInfo;
                    }
                }
            }

            return true;
        }

        public void loadingRanking()
        {
            foreach (string subject in thongTinMon.Keys)
            {
                tongSoDVHT += thongTinMon[subject].SoDVHT;
            }
            dataGridView2.Columns["DiemTB"].ToolTipText = "Điểm Trung Bình\nTổng Số Đơn Vị Học Trình: " + tongSoDVHT;

            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("MaLop", typeof(string));
            dataTable.Columns.Add("MaSV", typeof(string));
            dataTable.Columns.Add("HoTen", typeof(string));
            dataTable.Columns.Add("NgaySinh", typeof(string));
            dataTable.Columns.Add("DiemTB", typeof(string));

            DataTable dataTable_HocBong = new DataTable();
            dataTable_HocBong.Columns.Add("MaLop", typeof(string));
            dataTable_HocBong.Columns.Add("MaSV", typeof(string));
            dataTable_HocBong.Columns.Add("HoTen", typeof(string));
            dataTable_HocBong.Columns.Add("NgaySinh", typeof(string));
            dataTable_HocBong.Columns.Add("DiemTB", typeof(string));

            foreach (StudentInfo stdInfo in DsSV.Values)//Tinh diem cho moi sinh vien
            {

                int j = 0;
                object[] rowCells = new object[dataGridView2.Columns.Count];
                rowCells[j++] = stdInfo.MaLop;//MaLop
                rowCells[j++] = stdInfo.MaSV;//MaSV
                rowCells[j++] = stdInfo.TenSV;//HoTen
                rowCells[j++] = stdInfo.NgaySinh;//HoTen

                //Tinh Tong Diem
                float tongDiem = 0;
                float soDVHT = tongSoDVHT;
                foreach (KeyValuePair<string, MarkStructure> sbj in stdInfo.DS_MonHoc)
                {
                    tongDiem = tongDiem + (sbj.Value.DiemTK * thongTinMon[sbj.Key].SoDVHT);
                }

                rowCells[j++] = string.Format("{0:0.00}", (tongDiem / soDVHT));

                dataTable.Rows.Add(rowCells);

                //Kiểm tra điều kiện đạt học bổng
                bool datHocBong = true;
                foreach (KeyValuePair<string, MarkStructure> subject in stdInfo.DS_MonHoc)
                {
                    if (subject.Value.DiemTK < 5.0)
                    {
                        datHocBong = false;
                        break;
                    }
                }
                if (tongDiem / soDVHT >= 7.0 && datHocBong)
                {
                    dataTable_HocBong.Rows.Add(rowCells);
                }
            }
            BindingSrc.DataSource = dataTable;
            BindingSrc_HocBong.DataSource = dataTable_HocBong;
            dataGridView2.DataSource = BindingSrc;

            //Sắp xếp từ cao xuống thấp theo Điểm TB
            //dataGridView2.Sort(dataGridView2.Columns["DiemTB"], ListSortDirection.Descending);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            dataGridView1.Refresh();

            dataGridView2.Rows.Clear();
            dataGridView2.Refresh();

            danhSachMon.Clear();
            thongTinMon.Clear();
            tongSoDVHT = 0;

            if (loadingDataFromFile())
            {
                loadingRanking();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (folderBrowserDialog1)
            {
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    string selectedPath = folderBrowserDialog1.SelectedPath;

                    textBox1.Text = selectedPath;
                    dataPath = selectedPath;
                }
            }
        }

        private void textBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show(textBox1.Text, textBox1, 0, 20, 3000);
        }

        private void doFilter()
        {
            if (BindingSrc == null) return;
            BindingSrc.RemoveFilter();
            string searchValue = textBoxSearch.Text;
            string option1 = comboBox1.SelectedItem.ToString();
            string option2 = comboBox2.SelectedItem != null ? comboBox2.SelectedItem.ToString() : "";

            string filterString = "";
            BindingSource bSrc = option1.Equals("Học Bổng") ? BindingSrc_HocBong : BindingSrc;

            if (option1.Equals("Lớp"))
            {
                if (!option2.Equals("Tất Cả"))
                {
                    filterString = "MaLop = '" + option2 + "'";
                }
            }
            bSrc.Filter = (!filterString.Equals("") ? filterString + " AND" : "") + "(HoTen LIKE '%" + searchValue + "%' OR MaSV LIKE '%" + searchValue + "%')";
            dataGridView2.DataSource = bSrc;
            showMarkDetail();
        }

        private void showMarkDetail()
        {
            if (dataGridView2.SelectedCells.Count > 0)
            {
                int selectedRowIndex = dataGridView2.SelectedCells[0].RowIndex;
                long maSV = long.Parse(dataGridView2.Rows[selectedRowIndex].Cells["MaSV"].Value.ToString());
                StudentInfo stdInfo = DsSV[maSV];
                if (stdInfo != null)
                {
                    textBoxMaSV.Text = stdInfo.MaSV.ToString();
                    textBoxLop.Text = stdInfo.MaLop;
                    textBoxHoTen.Text = stdInfo.TenSV;
                    textBoxNgaySinh.Text = stdInfo.NgaySinh;

                    DataTable dataTable = new DataTable();
                    dataTable.Columns.Add("MaMon", typeof(string));
                    dataTable.Columns.Add("TenMon", typeof(string));
                    dataTable.Columns.Add("DVHT", typeof(int));
                    dataTable.Columns.Add("DiemCC", typeof(float));
                    dataTable.Columns.Add("DiemGK", typeof(float));
                    dataTable.Columns.Add("DiemHK", typeof(float));
                    dataTable.Columns.Add("DiemTK", typeof(float));

                    foreach (KeyValuePair<string, MarkStructure> subject in stdInfo.DS_MonHoc)
                    {

                        int i = 0;
                        object[] rowData = new object[dataTable.Columns.Count];
                        rowData[i++] = thongTinMon[subject.Key].MaMon;
                        rowData[i++] = thongTinMon[subject.Key].TenMon;
                        rowData[i++] = thongTinMon[subject.Key].SoDVHT;
                        rowData[i++] = subject.Value.DiemCC;
                        rowData[i++] = subject.Value.DiemGK;
                        rowData[i++] = subject.Value.DiemHK;
                        rowData[i++] = subject.Value.DiemTK;

                        dataTable.Rows.Add(rowData);
                    }
                    dataGridView1.DataSource = dataTable;
                }
            }
        }

        private void dataGridView2_Sorted(object sender, EventArgs e)
        {
            showMarkDetail();
        }

        private void dataGridView2_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            showMarkDetail();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem == "Lớp")
            {
                comboBox2.Enabled = true;
            }
            else
            {
                comboBox2.Enabled = false;
                comboBox2.SelectedItem = "Tất Cả";
            }
            doFilter();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            doFilter();
        }

        private void textBoxSearch_TextChanged(object sender, EventArgs e)
        {
            doFilter();
        }

        private void dataGridView2_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex == -1 && e.RowIndex > -1)
            {
                e.PaintBackground(e.CellBounds, true);
                using (SolidBrush br = new SolidBrush(Color.Black))
                {
                    StringFormat sf = new StringFormat();
                    sf.Alignment = StringAlignment.Center;
                    sf.LineAlignment = StringAlignment.Center;
                    e.Graphics.DrawString((e.RowIndex + 1).ToString(),
                        e.CellStyle.Font, br, e.CellBounds, sf);
                }
                e.Handled = true;
            }
        }
    }
}
